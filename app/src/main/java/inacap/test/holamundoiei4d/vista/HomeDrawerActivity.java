package inacap.test.holamundoiei4d.vista;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import inacap.test.holamundoiei4d.MainActivity;
import inacap.test.holamundoiei4d.R;
import inacap.test.holamundoiei4d.modelo.sqlite.HolaMundoDBContract;
import inacap.test.holamundoiei4d.vista.fragmentos.BienvenidaFragment;
import inacap.test.holamundoiei4d.vista.fragmentos.MapaFragments;
import inacap.test.holamundoiei4d.vista.fragmentos.otrofragmento;

public class HomeDrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, BienvenidaFragment.OnFragmentInteractionListener,otrofragmento.OnFragmentInteractionListener, MapaFragments.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_drawer);

        Toast.makeText(getApplicationContext(), "Bienvenido", Toast.LENGTH_SHORT).show();

        // Llamamos a la barra de herramientas
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Llamamos al boton flotante en la parte inferior de la ventana
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Mensaje temporal (como el toast), se muestra en la parte baja de la pantalla
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        // Componente que permite la apertura del panel lateral
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        // Mostar panel lateral
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        // Escuchamos cuando la persona seleccione un item del panel lateral
        navigationView.setNavigationItemSelectedListener(this);

        //Llamar a la cabecera
        View cabecera = navigationView.getHeaderView(0);

        //Llamar al campo de texto (al textview)
        TextView tvUsername = (TextView) cabecera.findViewById(R.id.tvUsername);

        //Tomar el nombre de usuario de la preferencia compartida
        SharedPreferences sesion = getSharedPreferences(HolaMundoDBContract.HolaMundoSesion.SHARED_PREFERECENCES_NAME, Context.MODE_PRIVATE);
        String username = sesion.getString(HolaMundoDBContract.HolaMundoSesion.FIELD_USERNAME, "");

        //Mostrar el dialogo en la pantalla nav
        tvUsername.setText("Bienvenido " + username);

        navigationView.setCheckedItem(R.id.nav_camera);
        navigationView.getMenu().performIdentifierAction(R.id.nav_camera, 0);

    }

    @Override
    public void onBackPressed() {

        //Manejamos el comportamiento del boton atras

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //Manejamos el comportamiento del menu de la barra de herramientas  (los 3 puntos)


        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.cerrar_sesion) {
            //Manejamos el cierre de sesion
            //Cerrar sesion
            SharedPreferences sesiones = getSharedPreferences(HolaMundoDBContract.HolaMundoSesion.SHARED_PREFERECENCES_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sesiones.edit();

            //Guardamos un boolean que diga si la persona inicio sesion
            editor.putBoolean(HolaMundoDBContract.HolaMundoSesion.FIELD_SESION, false);

            editor.commit();

            Intent nuevaVentana = new Intent(HomeDrawerActivity.this, MainActivity.class);
            startActivity(nuevaVentana);
            finish();

        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        //Reaccionamos a la accion del usuario
        //Buscamos la id del elemento seleccionado

        // Handle navigation view item clicks here.
        int id = item.getItemId();

        //crearemos un fragmento nulo

        Fragment fragments = null;

        if (id == R.id.nav_camera) {

            fragments = new MapaFragments();


        } else if (id == R.id.nav_gallery) {
            Toast.makeText(this, "Presionaste galeria", Toast.LENGTH_LONG).show();

        } else if (id == R.id.nav_slideshow) {
            fragments = new otrofragmento();
        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        if (fragments != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.flContent, fragments).commit();
        }


        setTitle(item.getTitle());









        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(String fragmentName, String action) {

        //Consultamos desde que fragmento viene un evento

        if(fragmentName.equals("otrofragmento")){
            if(action.equals("CONTADOR")){
                Toast.makeText(getApplicationContext(),"El contador cambio",Toast.LENGTH_LONG).show();
            }
        }

    }
}
