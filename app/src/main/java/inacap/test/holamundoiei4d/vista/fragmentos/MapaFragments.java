package inacap.test.holamundoiei4d.vista.fragmentos;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import inacap.test.holamundoiei4d.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MapaFragments.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MapaFragments#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MapaFragments extends Fragment implements OnMapReadyCallback {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    public int a = 1;


    private MapView mapview;
    GoogleMap mMmap;
    private OnFragmentInteractionListener mListener;

    public MapaFragments() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MapaFragments.
     */
    // TODO: Rename and change types and number of parameters
    public static MapaFragments newInstance(String param1, String param2) {
        MapaFragments fragment = new MapaFragments();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_mapa_fragments, container, false);

        //Preparamos el fragmento
        MapsInitializer.initialize(getActivity());

        //llamamos al componente del mapa
        mapview = (MapView) layout.findViewById(R.id.mapview);
        mapview.onCreate(savedInstanceState);
        mapview.getMapAsync(this);

        final FloatingActionButton BtCambiarMapa = (FloatingActionButton) layout.findViewById(R.id.btnCambiarMapa);
        BtCambiarMapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                a++;
                mMmap.setMapType(a);

                if (a == 4) {
                    a = 0;
                }

            }

        });


        return layout;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction("", "");
        }
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapview.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapview.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapview.onLowMemory();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mapview.onDestroy();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMmap = googleMap;
        LatLng Inacap = new LatLng(-36.5937083, -72.1038707);


        mMmap.moveCamera(CameraUpdateFactory.newLatLngZoom(Inacap, 13));
        mMmap.addMarker(new MarkerOptions()
                .title("Inacap")
                .snippet("Sede Chillan")
                .position(Inacap));
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String cambiaresto, String qwqe);
    }

}




